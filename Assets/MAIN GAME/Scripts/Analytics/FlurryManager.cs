﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FlurrySDK;

public class FlurryManager : MonoBehaviour
{
    public static FlurryManager Instance;

    public string FLURRY_API_KEY;


    void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

    }

    void Start()
    {
        // Initialize Flurry.
        new Flurry.Builder()
                  .WithCrashReporting(true)
                  .WithLogEnabled(true)
                  .WithLogLevel(Flurry.LogLevel.VERBOSE)
                  .WithMessaging(true)
                  .Build(FLURRY_API_KEY);

        Debug.Log("Initialize Flurry");

        LogStartEvent();
    }

    public void LogStartEvent()
    {
        Debug.Log("LogStartEvent()");
        var param = new Dictionary<string, string>();

        param["Level_start"] = (GameController.instance.LevelNumberGA + 1).ToString();

        Flurry.EventRecordStatus status = Flurry.LogEvent("Level_start", param, true);

    }

    public void LogArchivedEvent()
    {
        string key = (GameController.instance.LevelNumberGA + 1).ToString() + "_Archived";

        if (PlayerPrefs.GetInt(key) != 1)
        {
            Debug.Log("Level " + (GameController.instance.LevelNumberGA + 1).ToString() + " is archived!");

            var param = new Dictionary<string, string>();

            param["Level_archived"] = (GameController.instance.LevelNumberGA + 1).ToString();
            Flurry.EventRecordStatus status = Flurry.LogEvent("Level_archived", param, true);

            PlayerPrefs.SetInt(key, 1);
        }
        else
        {
            Debug.Log("Level " + (GameController.instance.LevelNumberGA + 1).ToString() + " has been archived already!");
        }

    }

}
